function Validate() {
    //validate ten
    let name = document.getElementById('name').value;
    let errName = document.getElementById('errName');
    let regexName = /^[a-zA-Z ]{2,30}$/;
    if (name == null || name == "") {
        errName.innerHTML = "Không được để trống!";
    }
    else if (!regexName.test(name)) {
        errName.innerHTML = "Không hợp lệ!";
    } else {
        errName.innerHTML = "";
    }
    //validate dia chi
    let Address = document.getElementById('Address').value;
    let errAddress = document.getElementById('errAddress');
    if (Address == null || Address == "") {
        errAddress.innerHTML = "Không được để trống!";
    } else {
        errAddress.innerHTML = "";
    }

    //validate so dien
    let Phone = document.getElementById('Phone').value;
    let errPhone = document.getElementById('errPhone');
    let regexPhone = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
    if (Phone == null || Phone == "") {
        errPhone.innerHTML = "Không được để trống!";
    } else if (!regexPhone.test(Phone)) {
        errPhone.innerHTML = "Không hợp lệ!";
    } else {
        errPhone.innerHTML = "";
    }

    // validate số lượng khachs
    let custom = document.getElementById('custom').value;
    let errCustom = document.getElementById('errCustom');
    if (custom == null || custom == "") {
        errCustom.innerHTML = "Không được để trống!";
    } else {
        errCustom.innerHTML = "";
    }

    let people = document.getElementById('people').value;
    let err = document.getElementById('error');
    let child = document.getElementById('child').value;
    if (child == null || child == "" || people == null || people == "") {
        err.innerHTML = "Không được để trống!";
    }
    else {
        err.innerHTML = "";
    }

    if (name && Address && Phone && custom && people && child) {
        document.getElementById('success').innerHTML = "Đăng ký thành công!!";
    }
    return false;
}